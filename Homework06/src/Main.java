public class Main {

    //this is function needed
    public static int   function(int[] array, int n) {
        int i;

        i = 0;
        while (i < array.length) {
            if (array[i] == n)
                return (i);
            i++;
        }
        return (-1);
    }

    //this is procedure needed
    public static void  procedure(int[] array) {
        int i;
        int j;
        int tmp;

        i = 0;
        i = array.length - 1;
        while (i >= 0) {
            j = array.length - 1;
            if (array[i] != 0)
                i--;
            else {
                while (array[j] == 0 && j >= 0)
                    j--;
                if (j >= 0) {
                    tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                }
            }
        }
        i = 0;
        while (i < array.length) {
            System.out.print(array[i] + " ");
            i++;
        }
    }
    //main for testing
    public static void main(String[] args) {
        int a[] = {0, 1, 2, 3, 4, 5};
        int b[] = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};

        System.out.println(function(a, 10));
        procedure(b);
    }
}
