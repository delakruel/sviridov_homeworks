package MapCollection;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        Map<String, Integer> map = new HashMap<>();
        String[] keys = str.split(" ");
        for (int i = 0; i < keys.length; i++) {
            if (map.containsKey(keys[i]))
                map.put(keys[i], map.get(keys[i]) + 1);
            else
                map.put(keys[i], 1);
        }
        System.out.println(map.entrySet());
    }
}
