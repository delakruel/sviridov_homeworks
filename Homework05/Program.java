import	java.util.Scanner;

class	Program {
	public static void main(String[] args) {
		Scanner	scanner = new Scanner(System.in);

		int	a = scanner.nextInt();
		int	min;
		int	b;

		if (a < 0)
			a = -a;
		min = a % 10;
		while (a != -1) {
			while (a != 0) {
				if (a < 0)
					a = -a;
				b = a % 10;
				a = a / 10;
				if (b < min)
					min = b;
			}
			a = scanner.nextInt();
		}

		System.out.println(min);
	}
}